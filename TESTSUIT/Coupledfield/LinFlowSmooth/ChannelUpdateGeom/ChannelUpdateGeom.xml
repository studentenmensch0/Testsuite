<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>channel flow on updated geometry (LinFlow-smooth)</title>
    <authors>
      <author>dmayrhof</author>
    </authors>
    <date>2021-12-10</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
          Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
   	  This test consists of a channel with three sections. The middle section is predeformed via the smoothPDE in a static step. Afterwards, the static deformation is used to compute the transient solution of the LinFlow-PDE in the deformed geometry. Due the static precomputation the initial velocity caused by the smoothPDE is zero. Hence, this testcase is used to check the geometry update of the integratos used in the LinFlow-PDE but also the correct initialization of the GLM-scheme based on an initial state.
    </description>
  </documentation>

  <fileFormats>
    <input>
      <!--<cdb fileName="ChannelUpdateGeom_live.cdb" gridId="default" id="h1"/>-->
      <hdf5 fileName="ChannelUpdateGeom.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList>
      <var name="h_d" value="100e-6"/>
      <var name="l" value="1e-3"/>
      <var name="h_air" value="500e-6"/>
    </variableList>
    <regionList>
      <region name="Air_1" material="FluidMat"/>
      <region name="Air_2" material="FluidMat"/>
      <region name="Air_3" material="FluidMat"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="ExcVel"/>
      <surfRegion name="ExcDisp"/>
      <surfRegion name="Fix_l"/>
      <surfRegion name="Fix_r"/>
      <surfRegion name="Fix_b"/>
      <surfRegion name="Fix_Air_t"/>
      <surfRegion name="Fix_Air_r"/>
      <surfRegion name="Fix_Air_b"/>
    </surfRegionList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="velPolyId">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="presPolyId">
      <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>
  
  <integrationSchemeList>
    <scheme id="velIntegId">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="presIntegId">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep index="1"> 
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air_2"/>
        </regionList>
        
        <bcsAndLoads>
          <fix name="Fix_l">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <fix name="Fix_b">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <fix name="Fix_r">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="ExcDisp">
            <comp dof="y" value="-h_d*sin(x/l*pi)"/>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </smooth>
    </pdeList>
  </sequenceStep>
  
  <sequenceStep index="2"> 
    <analysis>
      <transient>
        <numSteps>5</numSteps>
        <deltaT>5e-6</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Air_2"/>
        </regionList>
        
        <initialValues>
          <initialState>
            <sequenceStep index="1" extrapolateStatic="yes"/>
          </initialState>
        </initialValues>
        
        <bcsAndLoads>
          <fix name="Fix_l">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <fix name="Fix_b">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <fix name="Fix_r">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="ExcDisp">
            <comp dof="y" value="-h_d*sin(x/l*pi)"/>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>
          <nodeResult type="smoothVelocity">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </smooth>
      
      <fluidMechLin formulation="compressible" presPolyId="presPolyId" velPolyId="velPolyId">
        <regionList>
          <region name="Air_1"/>
          <region name="Air_2"/>
          <region name="Air_3"/>
        </regionList>
        
        <bcsAndLoads>    
          <velocity name="ExcVel">
            <comp dof="x" value="sin(2*pi*10000*t)*sin(-y/h_air)*(1-exp(-t/(3e-4)))"/>
          </velocity>
          <velocity name="ExcDisp">
            <coupling pdeName="smooth">
              <quantity name="smoothVelocity"/>
            </coupling>
          </velocity>
          <noSlip name="Fix_Air_b">     
            <comp dof="y"/>
          </noSlip>
          <noSlip name="Fix_Air_t">     
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechMeshVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <couplingList>
      <iterative PDEorder="mechanic;smooth">
        <convergence logging="yes" maxNumIters="2" stopOnDivergence="no">
          <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
          <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechPressure" value="1e-3" normType="rel"/>
        </convergence>
        <geometryUpdate>
          <region name="Air_2"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix storage="sparseNonSym"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
            <posDef>no</posDef>
            <symStruct>no</symStruct>
            <logging>yes</logging>
          </pardiso>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
  
</cfsSimulation>
