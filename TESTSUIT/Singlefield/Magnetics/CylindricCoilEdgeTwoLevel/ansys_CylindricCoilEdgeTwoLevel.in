! ===========================================================================
!  ANSYS input script for quarter-symmetric model of
!  air-filled, cylindric coil
! ===========================================================================
!
fini
/clear
/filname,CylindricCoilEdge
/prep7
/uis,msgpop,2      

! initialize macros for .mesh-interface
init

! -----------------------
!  geometric properties
! -----------------------

! element size
h = 5e-3

! inner radius of coil
r = 5e-3

! thickness of coil
t = 0.1e-3

! length of coil
l_coil = 20e-3

! width of enclosing air domain
w = 30e-3

! depth of setup
d = 50e-3

! type of mesh
!   1: hexahedral dominant mesh
!   2: tetrahedral mesh
!   3: mixed mesh (tet, wege, prism, pyra)
MESH_TYPE = 1

! -------------------
!  geometry creation
! -------------------
! generate keypoints for rotation
k,1,0,0,0
k,2,0,0,1

k,3,r-t/2,0
k,4,r+t/2,0
k,5,w,0

lrotat,3,4,5,,,,1,2,90,1
a,3,4,7,6
a,4,5,8,7
! auxiliary point


k,10,0,0,0
l,10,3
l,6,10
al,8,1,9


! generate remaining rectangle
!rectng,0,w,0,w

!allsel
!aovlap,all

! generate mesh (depending on type)
*if,MESH_TYPE,eq,1,THEN
  ! ----------------
  ! HEXAHEDRAL MESH
  ! ----------------
  setelems,'quadr','quad'

  ! refine mesh along lines
  !lesize,1,,,2
  !lesize,4,,,2
  
  !lesize,2,h/2
  !lesize,3,h/2


  asel,s,,1
  esize,h*1.3
  mshkey,1
  amesh,1
  
  mshkey,2
  allsel
  esize,h
  amesh,all
  
  ! extrude geometry
  setelems,'brick','quad'
  
  ! 1) thickness of coil
  esize,,l_coil/(h*3)
  !esize,,1
  allsel
  setelems,'brick','quad'
  vext,all,,,,,l_coil

  ! 2) air dephts of setup in z-direction
  esize,,(d-l_coil)/(h*3)
  asel,s,loc,z,l_coil
  setelems,'brick','quad'
  vext,all,,,,,(d-l_coil)
  
  ! generate surface mesh on all sides
  asel,s,ext
  setelems,'quadr','quad'
  amesh,all
*elseif,MESH_TYPE,eq,2,THEN
  
  ! ----------------
  ! TETRAHEDAL MESH
  ! ----------------

  ! extrude geometry
  ! 1) thickness of coil
  allsel
  vext,all,,,,,t
  
  ! 2) air dephts of setup in z-direction
  asel,s,loc,z,t
  vext,all,,,,,(d-t)
  
  ! generate mesh
  setelems,'tetra'
  esize,h*2
  vmesh,all
  
  ! generate surface mesh on all sides
  asel,s,ext
  setelems,'triangle'
  amesh,all

*elseif,MESH_TYPE,eq,3,THEN
  ! ----------------------------------
  ! MIXED MESH (TET, WEGE, HEX, PYRA)
  ! ----------------------------------
  setelems,'quadr'

  ! refine mesh along lines
  lesize,1,,,2
  lesize,4,,,2
  
  lesize,2,h/2
  lesize,3,h/2

  asel,s,,,3
  esize,h/2
  amesh,all
  
  allsel
  esize,h
  amesh,all
  
  ! extrude geometry
  setelems,'brick','quad'
  
  ! 1) thickness of coil
  !esize,,t/h
  esize,,1
  allsel
  setelems,'brick','quad'
  vext,all,,,,,t
  
  ! 2) air dephts of setup in z-direction
  ! prevent creation of hex/wedge elements
  esize,,0 
  asel,s,loc,z,t
  setelems,'brick','quad'
  vext,all,,,,,(d-t)
  
  ! generate volume mesh
  mopt,pyra,on
  setelems,'brick','quad'
  mshape,1
  vmesh,all
  
  ! generate surface mesh on all sides
  asel,s,ext
  setelems,'quadr','quad'
  amesh,all
*endif
csys,1
allsel
lsel,s,loc,x,r-t/2,r+t/2
asll,s,1
vsla,s,1
vsel,r,loc,z,0,l_coil
cm,coil,volume

lsel,s,loc,x,0,r-t/2
asll,s,1
vsla,s,1
vsel,r,loc,z,0,l_coil
cm,core,volume


csys,0
allsel
cmsel,u,coil
cmsel,u,core
cm,air,volume

! be careful and compress element numbers
allsel
numcmp,node
numcmp,elem

! write volume mesh
cmsel,s,coil
eslv
welems,'coil'

cmsel,s,core
eslv
welems,'core'

cmsel,s,air
eslv
welems,'air'

! write surface mesh
csys,0
allsel
asel,s,loc,x,0
esla
welems,'x-inner'

csys,1
allsel
asel,s,loc,x,w
esla
welems,'x-outer'

csys,0
allsel
asel,s,loc,y,0
esla
welems,'y-inner'

allsel
asel,s,loc,z,0
esla
welems,'z-inner'

allsel
asel,s,loc,z,d
esla
welems,'z-outer'

! select elements at inner z-axis for writing magnetic field
allsel
nsel,s,loc,x,0
nsel,r,loc,y,0
esln
eslv,r
cm,histelems,elem
wsavelem,'hist'

! select start / end faces of coil
cmsel,s,coil
aslv
asel,r,loc,x,0
esla
wsavelem,'coil-front'

cmsel,s,coil
aslv
asel,r,loc,y,0
esla
wsavelem,'coil-back'

! write out all nodes
allsel
wnodes

! write out mesh
mkhdf5
