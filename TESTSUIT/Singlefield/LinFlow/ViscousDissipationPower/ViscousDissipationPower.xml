<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
 xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>Viscous dissipation power test</title>
    <authors>
      <author>Dominik Mayrhofer, Hamideh Hassanpour</author>
    </authors>
    <date>2023-03-29</date>
    <keywords>
      <keyword>flow</keyword>
    </keywords>
    <references></references>
    <isVerified>no</isVerified>
    <description>
        This test is based on the test ViscousDissipationTotalStrainPart but here we use more elements to get a dissipation density field that is resolved more finely.
        Here we test the summation of both parts (overall dissipation power density) as the integrated dissipation power and compare it to the manual summation of the element results weighed by their respective volume.
        The reference result has been roughly calculated via Paraview (histogram in the state file) and summed up manually: (8*2.1e6+9*5.6e6+20*9.1e6+13*1.25e7+28*1.6e7+18*1.95e7+2*3e7+2*3.5e7) W/m^3 * (5e-7*2e-7*1) m^3 = 1.3407e-04 W
    </description>
  </documentation>
  <fileFormats>
    <input>
      <!-- specify you mesh input file here -->
      <!--<cdb fileName="recsurf.cdb"/>-->
      <hdf5 fileName="ViscousDissipationPower.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/><!-- the default output, i.e. the results_hdf5/job.cfs file -->
      <text id="txt"/><!-- you can also use text output (suitable for selected results, specify outputIds="txt" for them) -->
    </output>
    <materialData file="mat.xml" format="xml"/><!-- material database -->
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList>
      <var name="P" value="1"/>
    </variableList>
    <regionList>
      <!-- specify material assignment here -->
      <region name="S_flow" material="FluidMat"/><!-- 'name' refers to the name of the 'Block' defined in Trelis, 'material' refers to the name of the material in the mat.xml file -->
    </regionList> 
  </domain>
  
  <fePolynomialList>
    <Legendre id="velPolyId">
      <isoOrder>2</isoOrder> 
    </Legendre>
    
    <Legendre id="presPolyId">
      <isoOrder>1</isoOrder> 
    </Legendre>
  </fePolynomialList>
    
  <sequenceStep index="1"> 
    <analysis> 
      <harmonic>
        <frequencyList>
          <freq value="10e+3"/>
        </frequencyList>
      </harmonic>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="presPolyId" velPolyId="velPolyId">
        <regionList >
          <region name="S_flow"/>
        </regionList>
        
        <bcsAndLoads>
          <normalTraction  name="L_left" volumeRegion="S_flow" value="1"/>
          <velocity  name="L_top" >
            <comp dof="y" value="1"/>
          </velocity>
          <noSlip name="L_bottom">
            <comp dof="y"/>
            <comp dof="x"/>
          </noSlip>
        </bcsAndLoads>
        
        <storeResults >
          <elemResult type="fluidMechViscousDissipationDensity">
            <allRegions/>
          </elemResult>
          <regionResult type="fluidMechViscousDissipation">
            <allRegions/>
          </regionResult>
          <elemResult type="fluidMechViscousDissipationDensityTotalStrainPart">
            <allRegions/>
          </elemResult>
          <regionResult type="fluidMechViscousDissipation">
            <allRegions outputIds="h5,txt"/>
          </regionResult>
          <nodeResult  type="fluidMechPressure">
            <allRegions/>   
          </nodeResult>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
